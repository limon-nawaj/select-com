Selectcom::Application.routes.draw do
  # get "admins/new"
  # get "dashboard/index"

  root 'dashboard#index'
  resources :jobs
  resources :clients
  resources :sessions
  resources :teches
  resources :admins
  resources :sale_orders
  resources :attaches
  resources :tech_jobs
  resources :notes
  
  match '/signin',      to: 'sessions#new',           via: 'get'
  match '/signout',     to: 'sessions#destroy',       via: 'delete'
  match '/new_job',    to: 'jobs#new',               via: 'delete'
end
