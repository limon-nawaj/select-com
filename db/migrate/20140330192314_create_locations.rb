class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
    	t.integer 	:client_id
    	t.string 	  :site
    	t.string 	  :fax
    	t.string 	  :phone
    	t.string 	  :url
    	t.string 	  :address
      t.integer   :client_address
      t.timestamps
    end
  end
end
