class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
    	t.integer  :client_id
  		t.date     :start_date
  		t.date     :end_date
  		t.datetime :start_time
  		t.datetime :end_time
      t.string   :status
      t.timestamps
    end
  end
end
