class CreateClientReps < ActiveRecord::Migration
  def change
    create_table :client_reps do |t|
    	t.integer :client_id
      t.string :name
    	t.string :contact
    	t.string :office
    	t.string :cell
    	t.string :email
    	t.string :position
      	t.timestamps
    end
  end
end
