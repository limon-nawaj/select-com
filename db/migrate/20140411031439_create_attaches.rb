class CreateAttaches < ActiveRecord::Migration
  def change
    create_table :attaches do |t|
    	t.integer :job_id
    	t.string  :attach_type
    	t.string  :description
    	t.string  :document
    	t.boolean :admin_only, default: false
        t.timestamps
    end
  end
end
