class CreateSaleOrders < ActiveRecord::Migration
  def change
    create_table :sale_orders do |t|
    	t.integer :job_id
    	t.string  :sales_order
    	t.string  :purchase_order
    	t.string  :item
    	t.integer :quantity
    	t.decimal :price, :precision => 8, :scale => 2
      t.timestamps
    end
  end
end
