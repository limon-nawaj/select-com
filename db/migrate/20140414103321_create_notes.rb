class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
    	t.integer :job_id
    	t.integer :user_id
    	t.string :note
    	t.boolean :notify, default: false
      	t.timestamps	
    end
  end
end
