class CreateTechJobs < ActiveRecord::Migration
  def change
    create_table :tech_jobs, id: false do |t|
    	t.integer  :job_id
    	t.integer  :tech_id
    	t.date 	   :start_date
    	t.datetime :start_time
    	t.date 	   :end_date
    	t.datetime :end_time
    	t.boolean  :disable_start, default: false
    	t.boolean  :work_status, default: false
        t.timestamps
    end
    add_index :tech_jobs, :job_id
    add_index :tech_jobs, :tech_id
  end
end
