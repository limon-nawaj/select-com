class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
    	t.string     :first_name
    	t.string     :last_name
    	t.string     :contact
    	t.string     :address
    	t.string     :city
    	t.attachment :photo
		t.timestamps
    end
  end
end
