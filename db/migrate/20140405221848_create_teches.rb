class CreateTeches < ActiveRecord::Migration
  def change
    create_table :teches do |t|
    	t.string       :first_name
    	t.string       :last_name
    	t.string       :contact
    	t.string       :address
    	t.string       :city
    	t.attachment   :photo
        t.date         :start_date
		t.timestamps
    end
  end
end
