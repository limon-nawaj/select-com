class CreateSiteContacts < ActiveRecord::Migration
  def change
    create_table :site_contacts do |t|
   		t.integer :client_id
   		t.string  :contact
    	t.string  :office
    	t.string  :cell
    	t.string  :email
    	t.string  :position
      t.timestamps
    end
  end
end
