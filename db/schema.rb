# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140414103321) do

  create_table "admins", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "contact"
    t.string   "address"
    t.string   "city"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "attaches", force: true do |t|
    t.integer  "job_id"
    t.string   "attach_type"
    t.string   "description"
    t.string   "document"
    t.boolean  "admin_only",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "client_reps", force: true do |t|
    t.integer  "client_id"
    t.string   "name"
    t.string   "contact"
    t.string   "office"
    t.string   "cell"
    t.string   "email"
    t.string   "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clients", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "fax"
    t.string   "url"
    t.string   "address"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.integer  "client_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.integer  "client_id"
    t.string   "site"
    t.string   "fax"
    t.string   "phone"
    t.string   "url"
    t.string   "address"
    t.integer  "client_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notes", force: true do |t|
    t.integer  "job_id"
    t.integer  "user_id"
    t.string   "note"
    t.boolean  "notify",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sale_orders", force: true do |t|
    t.integer  "job_id"
    t.string   "sales_order"
    t.string   "purchase_order"
    t.string   "item"
    t.integer  "quantity"
    t.decimal  "price",          precision: 8, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "site_contacts", force: true do |t|
    t.integer  "client_id"
    t.string   "contact"
    t.string   "office"
    t.string   "cell"
    t.string   "email"
    t.string   "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tech_jobs", id: false, force: true do |t|
    t.integer  "job_id"
    t.integer  "tech_id"
    t.date     "start_date"
    t.datetime "start_time"
    t.date     "end_date"
    t.datetime "end_time"
    t.boolean  "disable_start", default: false
    t.boolean  "work_status",   default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tech_jobs", ["job_id"], name: "index_tech_jobs_on_job_id"
  add_index "tech_jobs", ["tech_id"], name: "index_tech_jobs_on_tech_id"

  create_table "teches", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "contact"
    t.string   "address"
    t.string   "city"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.date     "start_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "role"
    t.string   "remember_token"
    t.integer  "client_id"
    t.integer  "tech_id"
    t.integer  "admin_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["admin_id"], name: "index_users_on_admin_id"
  add_index "users", ["client_id"], name: "index_users_on_client_id"
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["remember_token"], name: "index_users_on_remember_token"
  add_index "users", ["tech_id"], name: "index_users_on_tech_id"

end
