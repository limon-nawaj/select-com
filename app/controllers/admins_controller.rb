class AdminsController < ApplicationController
	layout "application_login"
  def new
  	@admin = Admin.new
  	@admin.build_user unless @admin.user
  end

  def create
  	@admin = Admin.new(admin_params)
  	if @admin.save
  		sign_in @admin
	    flash[:success] = "Admin created successfully and Loged in"
	    redirect_to root_path
  	else
  		render 'new'
  	end
  end

  private
  	def admin_params
  		params.require(:admin).permit(
    			:first_name, :last_name, :contact, :photo, :city,
    			user_attributes: [
  				:id,
  				:email,
  				:password,
  				:password_confirmation,
  				:role
  			]
  		)
  	end
end
