class SaleOrdersController < ApplicationController
respond_to :html, :xml, :js
	# def new
	# 	@client = Client.new
	# 	@client.locations.build
	# 	@client.site_contacts.build
	# 	@client.client_reps.build
	# 	@client.jobs.build
	# 	@client.build_user unless @client.user

	# 	@sale_order = SaleOrder.new
	# end

	def create
		@sale_order = SaleOrder.new(sale_order_params)
		if @sale_order.job_id.blank?
			flash[:success] = 'No job found'
			redirect_to new_job_path	
		else
			if @sale_order.save
				flash[:success] = "Sales order saved successfully"
				@job = Job.find_by(id: @sale_order.job_id)
				redirect_to edit_job_path(@job)
			else
				flash[:success] = 'No sales order is found'
				redirect_to edit_job_path(@sale_order.job_id)
			end		
		end
	end

	def edit
		@job = Job.find(params[:id])
		@client = @job.client
	end

	private
		def sale_order_params
			params.require(:sale_order).permit(
				:job_id,
				:sales_order,
				:purchase_order,
				:item,
				:quantity,
				:price
			)
		end
end
