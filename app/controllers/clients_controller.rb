class ClientsController < ApplicationController

	# before_action :set_client, only: [:update]

	def index
		@clients = Client.paginate(page: params[:page])
	end


	def create
		@client = Client.new(client_params)
		if @client.save
			@job = Job.find_by(client_id: @client.id)
			flash[:success] = "Client added successfully"
			redirect_to edit_job_path(@job)
		else
			render 'jobs/new'
		end
	end

	# def edit
	# 	@job = Job.find_by(params[:id])
	# 	@client = @job.client
	# end

	def update
		@job = Job.find(params[:id])
		@client = @job.client
		if @client.update_attributes(client_params)
			flash[:success] = "Client updated successfully"
			redirect_to edit_job_path(@job)
		else
			render 'edit'
		end
	end

	private

		def client_params
			params.require(:client).permit(
				:name,
				:phone,
				:fax,
				:url,
				:address,
				:city,
				user_attributes: [
					:id,
					:email,
					:password,
					:password_confirmation,
					:role
				],
				locations_attributes: [
					:id,
					:site,
					:fax,
					:phone,
					:url,
					:address
				],
				site_contacts_attributes: [
					:id,
					:contact,
					:office,
					:cell,
					:email,
					:position
				],
				client_reps_attributes: [
					:id,
					:name,
					:contact,
					:office,
					:cell,
					:email,
					:position
				],
				jobs_attributes: [
					:id,
					:start_date,
					:end_date,
					:start_time,
					:end_time,
					:status,
					sale_order_attributes: [
						:id,
						:sales_order,
						:purchase_order,
						:item,
						:quantity,
						:price
					]
				]

			)
		end

		def update_client_params
			params.require(:client).permit(
				:name,
				:phone,
				:fax,
				:url,
				:address,
				:city,
				user_attributes: [
					:id,
					:email,
					:password,
					:password_confirmation,
					:role
				],
				locations_attributes: [
					:id,
					:site,
					:fax,
					:phone,
					:url,
					:address
				],
				site_contacts_attributes: [
					:id,
					:contact,
					:office,
					:cell,
					:email,
					:position
				],
				client_reps_attributes: [
					:id,
					:name,
					:contact,
					:office,
					:cell,
					:email,
					:position
				]
			)
		end
end
