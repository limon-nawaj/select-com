class LocationsController < ApplicationController
	def update
		@locaiton = Location.find(params[:id])
		if @location.update_attributes(location_params)
			flash[:success] = "Location updated successfully"
		end
	end

	private
		def location_params
			params.require(:location).permit(
				:client_id,
				:site,
				:fax,
				:phone,
				:url,
				:address
			)
		end
end
