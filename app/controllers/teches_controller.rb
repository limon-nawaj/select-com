class TechesController < ApplicationController
	before_action :set_tech, only: [:edit, :update]

	def index
		@teches = Tech.paginate(page:params[:page])
  	end

	def new
		@tech = Tech.new
		@tech.build_user unless @tech.user
	end

	def create
		@tech = Tech.new(tech_params)
		if @tech.save
			flash[:success] = "Tech added successfully"
			redirect_to teches_path
		else
			render 'new'
		end
	end

	def edit
	end

	def update 
		if @tech.update(tech_params)
	 		flash[:success] = "Tech updated successfully"
	 		redirect_to teches_path
	 	else
	 		render 'edit'
	 	end 	
	end

	private 
	  	def set_tech
	  		@tech = Tech.find(params[:id])
	  	end

	  	def tech_params
	  		params.require(:tech).permit(
	  			:first_name, :last_name, :contact, :city, :photo, :start_date,
	  			user_attributes: [
					:id,
					:email,
					:password,
					:password_confirmation,
					:role
				]
	  		)
	  	end
end
