class JobsController < ApplicationController
	before_action :signed_in_user
		
	def index
		@jobs = Job.paginate(page: params[:page])
	end

	def new
		@client = Client.new
		@client.locations.build
		@client.site_contacts.build
		@client.client_reps.build
		@job = @client.jobs.build
		@job.build_sale_order unless @job.sale_order
		@client.build_user unless @client.user


		@sale_order = SaleOrder.new

		@attach = Attach.new
		@attaches = @job.attaches
		@tech_job = TechJob.new
		@note = Note.new
	end

	def edit
		@job = Job.find(params[:id])
		@client = Client.find_by(id: @job.id)
		@attach = Attach.new
		@attaches = @job.attaches
		@tech_job = TechJob.new
		@note = Note.new
	end

	def create
		# @job = Job.new(job_params)
		# if @job.blank?
		# 	flash[:error] = 'No job exist'
		# 	render 'new'
		# end
	end

	def update
		@job = Job.find(params[:id])
		if @job.update(job_params)
			flash[:success] = "updated successfully"
			redirect_to edit_job_path(@job)
		else
			render 'edit'
		end
	end

	private
		def job_params
			params.require(:job).permit(
				:start_date,
				:end_date,
				:start_time,
				:end_time,
				:status,
				sale_order_attributes: [
					:id,
					:sales_order,
					:purchase_order,
					:item,
					:quantity,
					:price
				],
				attaches_attributes: [
					:id,
					:attach,
					:description,
					:admin_only
				]
			)
		end
end
