class TechJobsController < ApplicationController
	def create
		@tech_job = TechJob.new(tech_job_params)
		if @tech_job.job_id.blank?
			flash[:success] = 'No job found'
			redirect_to new_job_path	
		else
			@job = @tech_job.job
			if @tech_job.save
				flash[:success] = 'Tech added successfully'
				redirect_to edit_job_path(@job)			
			else
				flash[:success] = 'Tech already added in this job'
				redirect_to edit_job_path(@tech_job.job_id)
			end
		end
	end

	private
		def tech_job_params
			params.require(:tech_job).permit(:job_id, :tech_id, :start_date, :start_time, :end_date, :end_time, :disable_start, :work_status)
		end
end
