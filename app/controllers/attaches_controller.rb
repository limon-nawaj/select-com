class AttachesController < ApplicationController
	def create
		@attach = Attach.new(attach_params)
		if @attach.job_id.blank?
			flash[:success] = 'Attach saved successfully'
			redirect_to new_job_path
		else
			if @attach.save
				flash[:success] = 'Attach saved successfully'
				@job = @attach.job
				redirect_to edit_job_path(@job)
			end	
		end
		
	end

	# def destroy
	# 	@attach = Attach.find(params[:id])
	# 	@job = @attach.job
	# 	@attach.destroy
	# 	flash[:success] = 'Document deleted successfully'
	# 	redirect_to edit_job_path(@job)
	# end

	private

		def attach_params
			params.require(:attach).permit(
				:id,
				:job_id,
				:attach_type,
				:description,
				:admin_only,
				:document
			)
		end
end
