$(document).ready(function(){
	$('#hide_client').click(function(){
		$('#client').hide(1000);
	});
	$('#show_client').click(function(){
		$('#client').show(1000);
	});

	$('#hide_location').click(function(){
		$('#location').hide(1000);
	});
	$('#show_location').click(function(){
		$('#location').show(1000);
	});

	$('#hide_site').click(function(){
		$('#site').hide(1000);
	});
	$('#show_site').click(function(){
		$('#site').show(1000);
	});

	$('#hide_rep').click(function(){
		$('#rep').hide(1000);
	});
	$('#show_rep').click(function(){
		$('#rep').show(1000);
	});


	$('#minimize').click(function(){
		$('#client').hide(1000);
		$('#location').hide(1000);
		$('#site').hide(1000);
		$('#rep').hide(1000);
		$('#rep').hide(1000);
	});
	$('#maximize').click(function(){
		$('#client').show(1000);
		$('#location').show(1000);
		$('#site').show(1000);
		$('#rep').show(1000);
		$('#rep').show(1000);
	});	

	$('#myModal').load();

	$('#datepicker').datepicker({
		format: 'mm-dd-yyyy'
	});

	$('#datepicker').datepicker().on("changeDate", function(e){ 
		$('#datepicker').datepicker('hide');
	});

	$('[data-behaviour~=datepicker]').datepicker();
	 
});

