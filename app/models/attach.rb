class Attach < ActiveRecord::Base
	belongs_to :job
	mount_uploader :document, DocumentUploader
end
