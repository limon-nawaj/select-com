class Client < ActiveRecord::Base
	has_many 	:locations, :dependent => :destroy
	has_many 	:site_contacts, :dependent => :destroy
	has_many 	:client_reps, :dependent => :destroy
	has_many    :jobs, :dependent => :destroy
	has_one     :user, :dependent => :destroy

	accepts_nested_attributes_for :locations, :allow_destroy => true
	accepts_nested_attributes_for :site_contacts, :allow_destroy => true
	accepts_nested_attributes_for :client_reps, :allow_destroy => true
	accepts_nested_attributes_for :jobs, :allow_destroy => true
	accepts_nested_attributes_for :user, :allow_destroy => true

	validates :name, presence: true
	validates :phone, presence: true
end
