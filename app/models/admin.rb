class Admin < ActiveRecord::Base
	has_one :user

	accepts_nested_attributes_for :user
	
	has_attached_file :photo, style: { :medium => "400x300>", :thumb => "100x100>" }
	validates_attachment :photo, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] }
end
