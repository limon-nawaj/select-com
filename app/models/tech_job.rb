class TechJob < ActiveRecord::Base
	belongs_to :job
	belongs_to :tech

	validates_uniqueness_of :job_id, :scope => :tech_id, :message => "can only have one tech assigned"
end
