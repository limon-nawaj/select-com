class Tech < ActiveRecord::Base
	has_one :user
	has_many :tech_jobs

	accepts_nested_attributes_for :user
	
	has_attached_file :photo, style: { :medium => "400x300>", :thumb => "100x100>" }
	# validates_attachment :photo, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] }

end
