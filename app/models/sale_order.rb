class SaleOrder < ActiveRecord::Base
	belongs_to :job
	# before_save { self.sales_order = sales_order.downcase }
	validates :sales_order, presence: true, uniqueness: { case_sensitive: false }
	# validates :job_id, presence: true
	
end
