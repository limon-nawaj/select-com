class Job < ActiveRecord::Base
	belongs_to :client
	has_one :sale_order
	has_many :attaches
	has_many :tech_jobs
	has_many :notes

	# validates :client_id, presence: true
	accepts_nested_attributes_for :sale_order
	accepts_nested_attributes_for :attaches
end
